#!/bin/bash

#############################
#
#       Author: J. Dias/S. Lankalapalli - 6/24/2022
#       Updated: 
#
#############################

set -e

# location of logs and names of logfiles all pertinent to Momentum
ECELERITY_LOG_DIR="/var/log/ecelerity"
MOMENTUM_LOG_FILES="acctlog.ec mainlog.ec bouncelog.ec rejectlog.ec paniclog.ec tlslog.ec httplog.ec ecconfigd.log"

NGINX_LOG_DIR="/var/log/msys-nginx"
NGINX_LOG_FILES="access.log error.log"

NODEJS_LOG_DIR="/var/log/msys-nodejs"
NODEJS_LOG_FILES="webhooks-api.js.log webhooks-etl.js.log webhooks-batch-status.log"

RABBITMQ_LOG_DIR="/var/log/msys-rabbitmq"
RABBITMQ_LOG_FILES="rabbit@edipvldepm01.log rabbit@edipvldepm01-sasl.log shutdown_err shutdown_log startup_err startup_log"


# how many local files to retain
MAXTORETAIN=10

# these are the two possible destinations
#old ROOT_AWS='s3://${SDLC}-depses.broadridge.com/BRCC-AWS'
ROOT_AWS='s3://br-ics${SDLC}-dep-stggw-us-east-1-s3/BRCC-AWS'
#br-icsdev-dep-stggw-us-east-1-s3
ROOT_ONPREM='/stgw_logs'
# mount cmd: mount -t nfs -o nolock,hard 10.22.7.23:/br-icsdev-dep-stggw-us-east-1-s3 /stgw_logs


# defaults
dt=`date +"%Y-%m-%d"`
rc=0
START=1
END=1
DEBUG=0
NOCRON=0
myName=`basename $0 .sh`
myPath="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"


usage() {
	echo "usage:"
	echo "$myName [-d] [-n #] [-s #] [-e #]"
	echo "$myName [--debug] [--number #] [--start #] [--end #]"
}


doerror() {
	echo "$myName: fail: " $2 ":" $1 
    exit $1
}


# handle command line options
PARSED_ARGUMENTS=$(getopt -o :?hdan:s:e:c --long help,debug,all,number,start,end,nocron -- "$@")
if [ $? -ne 0 ] ; then
        usage
		doerror 91 "invalid option"
fi

[ $DEBUG -ne 0 ] && echo "PARSED_ARGUMENTS is $PARSED_ARGUMENTS"
eval set -- "$PARSED_ARGUMENTS"

# cycle over options
while : ; do
	[ $DEBUG -ne 0 ] && echo option=$1
	case "$1" in
		# -- means the end of the arguments; drop this, and break out of the while loop
		--) shift; break ;;
		-d | --debug)	# debug
			set -vx
			;;
		-a | --all)	#all
			START=99
			END=1
			;;
		-n | --number)	# single number
			START=$2
			END=$2
			shift
			;;
		-s | --start)	# start
			START=$2
			shift
			;;
		-e | --end)	# end
			END=$2
			shift
			;;
		-c | --nocron) # crontab entry
			NOCRON=1
			shift
			;;
		-h | -? | --help)
			usage
			exit 0
			;;
	esac
[ $DEBUG -ne 0 ] && echo $1
	shift
done


docrontab() {
	[ ${NOCRON} -eq 1 ] && return 0

	set +e
	if [ -n "`crontab -l 2>/dev/null|grep ${myPath}/${myName}`" ] ; then
		set -e
		return 0
	fi
	
	# add to crontab
	( crontab -l 2>/dev/null ; echo "# send momentum logs to aws ${myPath}/${myName}.sh" ; echo "9 0 * * * ${myPath}/${myName}.sh" ) | crontab -
	rc=$?
	
	set -e
	return $rc
}


getSDLC() {
	# if SDLC is already defined, go ahead and use it.
	if [ -z "$SDLC"] ; then
		# based on hostname decide on what set of scripts to retrieve
		local SDLC=""
		if [ $ONPREM -eq 1 ] ; then
			local HOSTNAME=`(hostname -s 2>/dev/null || hostname 2>/dev/null) | tr [:upper:] [:lower:]`
			[ -z "${HOSTNAME}" ] && doerror 92 "unable to determine hostname"
			
			case "${HOSTNAME:0:1}" in
				"m")    # Markham/Montreal
						SDLC=${HOSTNAME:4:1}
#						TYPE=${HOSTNAME:10:1}
						;;
				"f")    # Frankfurt
						SDLC=${HOSTNAME:3:1}
#						TYPE=${HOSTNAME:10:1}
						;;
				"p")    # Paris
						SDLC=${HOSTNAME:4:1}
#						TYPE=${HOSTNAME:10:1}
						;;
				"c")    # Clifton
						SDLC=${HOSTNAME:3:1}
						[ "${SDLC}" == "x" ] && SDLC="p"
#						TYPE=${HOSTNAME:9:1}
						;;
				*)
						SDLC=${HOSTNAME:3:1}
#						TYPE=${HOSTNAME:9:1}
						;;
			esac
			case $SDLC in
			"d" | "dev" )
				SDLC="dev"
				;;
			"i" | "int")
				SDLC="int"
				;;
			"q" | "qa")
				SDLC="qa"
				;;
			"u" | "uat")
				SDLC="uat"
				;;
			"p" | "prd" | "prod")
				SDLC="prd"
				;;
			esac
		else
			local INSTANCE_ID="`curl -s -k http://instance-data/latest/meta-data/instance-id`"
			[ -z "${INSTANCE_ID}" ] && doerror 93 "running on ASWS but no INSTANCE_ID"

			# SDLC may have two names SDLCEnv or SDLC
			local TAG_NAME=SDLCEnv
			local SDLC="`aws ec2 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=$TAG_NAME" --output=text | cut -f5 | tr [:upper:] [:lower:]`"
			if [ -z "${SDLC}" ] ; then
				TAG_NAME=SDLC
				SDLC="`aws ec2 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=$TAG_NAME" --output=text | cut -f5 | tr [:upper:] [:lower:]`"
			fi
		fi
		if [ -z "${SDLC}" ] ; then
			doerror 94 "unable to determine SDLC"
		fi
	fi
	echo $SDLC
}


doarchive() {
	[ -z "$1" ] && return 1
	[ -e "$1" ] || return 2
	[ -z "$2" ] && return 3

	[ -z "${MAXTORETAIN}" ] && return 3
	[ ${MAXTORETAIN} -lt 2 ] && return 4

	rc=0
	local SRC=$1
	local SRC_FILE=`basename $1 | awk -F. '{print $1}'`

	local DST_DIR=`dirname $2`
	local DEST=$2
	
	if [ ! -d ${DST_DIR} ] ; then
		mkdir ${DST_DIR} || doerror $? "mkdir ${DST_DIR}"
	fi

	echo mv $SRC $DEST
	mv $SRC $DEST || rc=$?
	[ $rc -ne 0 ] && doerror $rc "mv $SRC $DEST"

	# now do the removal of extra files
	tot=`ls -1 $DST_DIR/${SRC_FILE}* 2>/dev/null | wc -l`
	[ -z "${tot}" ] && tot=0
	[ $tot -le ${MAXTORETAIN} ] && return 0

	todel=$(($tot - ${MAXTORETAIN}))
	ls -1 $DST_DIR/${SRC_FILE}* | sort -r | head -${todel} | xargs rm -f || rc=$?
	[ $rc -ne 0 ] && doerror $rc "failed to delete extra files"
	
	return 0
}


docopy() {
	[ -z "$1" ] && return 1
	[ -e "$1" ] || return 2
	[ -z "$2" ] && return 3

	rc=0
	if [ $ONPREM -eq 1 ] ; then
		if [ ! -d ${DESTFOLDER} ] ; then
			mkdir -p ${DESTFOLDER} || rc=$?
			[ $rc -ne 0 ] && doerror $rc "mkdir -p ${DESTFOLDER}"
		fi
		echo cp "$1" ${DESTFOLDER}/$2
		cp "$1" ${DESTFOLDER}/$2 || rc=$?
		[ $rc -ne 0 ] && doerror $rc "cp $1 ${DESTFOLDER}/$2"
	fi
	if [ $AWS -eq 1 ] ; then
		echo aws s3 cp "$1" ${DESTFOLDER}/$2
		aws s3 cp "$1" ${DESTFOLDER}/$2 || rc=$?
		[ $rc -ne 0 ] && doerror $rc "aws s3 cp $1 ${DESTFOLDER}/$2"
	fi

	return $rc
}


####################################################
#                    start here                    #
####################################################

# check START/END parameters
if [ $START -lt $END ] ; then
	usage
	doerror 95 "START($START) must be higher number than END($END)"
fi

# determine if we are ONPREM or AWS
if [ -n "`ps -ef | grep "awsagent/bin/awsagent" | grep -v grep`" ] ; then
	# running on AWS if awsagent is running
	aws --version >/dev/null 2>&1 || rc=$?
	if [ $rc -ne 0 ] ; then
		doerror $rc "awsagent running but awscli not installed/working."
	fi
	AWS=1
	ONPREM=0
	myHOSTNAME=`/usr/sbin/ip -o a | sort | tail -1 | awk '{sub("/.*", "", $4); print $4}'`
	SDLC="$(getSDLC)"
	ROOT=`eval echo ${ROOT_AWS}`
else
	# running onprem if no awsagent running
	AWS=0
	ONPREM=1
	myHOSTNAME=`(hostname -s 2>/dev/null || hostname 2>/dev/null) | tr [:upper:] [:lower:]`
	SDLC="$(getSDLC)"
	ROOT=${ROOT_ONPREM}
fi


# do crontab addition
docrontab
if [ $? -ne 0 ] ; then
        usage
		doerror 96 "failed to insert into crontab"
fi


# now do the work. look for recently rotated 
#ecelerity logs
for i in `seq $START -1 $END` ; do
	pushd ${ECELERITY_LOG_DIR} >/dev/null 2>&1 || doerror 97 "${ECELERITY_LOG_DIR} does not exist"

	for baseFname in ${MOMENTUM_LOG_FILES} ; do
		[ `ls -1 ${baseFname}.${i}.* 2>/dev/null | wc -l` -eq 0 ] && continue
		ext=`ls ${baseFname}.${i}.* | awk -F. '{print "."$NF}'`
		[ -e ${baseFname}.${i}${ext} ] || continue

		dname1=`echo $baseFname | awk -F. '{print $1}'`
		dname2=`echo $baseFname | awk -F. '{print $1 FS $2}'`
		DESTFOLDER="${ROOT}/`echo $SDLC | tr [:lower:] [:upper:]`/${myHOSTNAME}/logs/${dname1}"

		[ ${DEBUG} -ne 0 ] && echo docopy ${ECELERITY_LOG_DIR}/${baseFname}.${i}${ext} ${dname2}.${dt}${ext}
		docopy ${ECELERITY_LOG_DIR}/${baseFname}.${i}${ext} ${dname2}.${dt}${ext} || rc=$?
		[ $rc -ne 0 ] && doerror $rc "abort from docopy"

		[ ${DEBUG} -ne 0 ] && echo doarchive ${ECELERITY_LOG_DIR}/${baseFname}.${i}${ext} ${ECELERITY_LOG_DIR}/archive/${dname2}.${dt}${ext}
		doarchive ${ECELERITY_LOG_DIR}/${baseFname}.${i}${ext} ${ECELERITY_LOG_DIR}/archive/${dname2}.${dt}${ext} || rc=$?
		[ $rc -ne 0 ] && doerror $rc "abort from doarchive"
			
	done

	popd >/dev/null
done
[ ${DEBUG} -ne 0 ] && echo "================================================================================================================="

# msys-nginx
for i in `seq $START -1 $END` ; do
	pushd ${NGINX_LOG_DIR} >/dev/null 2>&1 || doerror 98 "nginx log does not exist"

	dname1=`basename \`pwd\``		
	DESTFOLDER="${ROOT}/`echo $SDLC | tr [:lower:] [:upper:]`/${myHOSTNAME}/logs/${dname1}"

	for j in ${NGINX_LOG_FILES} ; do
		[ `ls -1 ${j}-* 2>/dev/null | wc -l` -eq 0 ] && continue
		dname2=`ls -1 ${j}-* | sort | tail -${i} | head -1`
		
		[ ${DEBUG} -ne 0 ] && echo docopy ${NGINX_LOG_DIR}/${dname2} ${dname2}
		docopy ${NGINX_LOG_DIR}/${dname2} ${dname2} || rc=$?
		[ $rc -ne 0 ] && doerror $rc "abort from docopy"
		
		[ ${DEBUG} -ne 0 ] && echo doarchive ${NGINX_LOG_DIR}/${dname2} ${NGINX_LOG_DIR}/archive/${dname2}
		doarchive ${NGINX_LOG_DIR}/${dname2} ${NGINX_LOG_DIR}/archive/${dname2} || rc=$?
		[ $rc -ne 0 ] && doerror $rc "abort from doarchive"
		
	done
	
	popd >/dev/null
done
[ ${DEBUG} -ne 0 ] && echo "================================================================================================================="

# msys-nodejs
for i in `seq $START -1 $END` ; do
	pushd ${NODEJS_LOG_DIR} >/dev/null 2>&1 || doerror 99 "msys-nodejs log does not exist"

	dname1=`basename \`pwd\``
	DESTFOLDER="${ROOT}/`echo $SDLC | tr [:lower:] [:upper:]`/${myHOSTNAME}/logs/${dname1}"

	for j in ${NODEJS_LOG_FILES} ; do
		[ `ls -1 ${j}-* 2>/dev/null| wc -l` -eq 0 ] && continue
		dname2=`ls -1 ${j}-* | sort | tail -${i} | head -1`
		
		[ ${DEBUG} -ne 0 ] && echo docopy ${NODEJS_LOG_DIR}/${dname2} ${dname2}
		docopy ${NODEJS_LOG_DIR}/${dname2} ${dname2} || rc=$?
		[ $rc -ne 0 ] && doerror $rc "abort from docopy"
		
		[ ${DEBUG} -ne 0 ] && echo doarchive ${NODEJS_LOG_DIR}/${dname2} ${NODEJS_LOG_DIR}/archive/${dname2}
		doarchive ${NODEJS_LOG_DIR}/${dname2} ${NODEJS_LOG_DIR}/archive/${dname2} || rc=$?
		[ $rc -ne 0 ] && doerror $rc "abort from doarchive"

	done
	
	popd >/dev/null
done
[ ${DEBUG} -ne 0 ] && echo "================================================================================================================="

# msys-rabbitmq
for i in `seq $START -1 $END` ; do
	pushd ${RABBITMQ_LOG_DIR} >/dev/null 2>&1 || doerror 90 "msys-rabbitmq log does not exist"

	dname1=`basename \`pwd\``
	DESTFOLDER="${ROOT}/`echo $SDLC | tr [:lower:] [:upper:]`/${myHOSTNAME}/logs/${dname1}"

	for j in ${RABBITMQ_LOG_FILES} ; do
		[ `ls -1 ${j}-* 2>/dev/null| wc -l` -eq 0 ] && continue
		dname2=`ls -1 ${j}-* | sort | tail -${i} | head -1`
		
		[ ${DEBUG} -ne 0 ] && echo docopy ${RABBITMQ_LOG_DIR}/${dname2} ${dname2}
		docopy ${RABBITMQ_LOG_DIR}/${dname2} ${dname2} || rc=$?
		[ $rc -ne 0 ] && doerror $rc "abort from docopy"
		
		[ ${DEBUG} -ne 0 ] && echo doarchive ${RABBITMQ_LOG_DIR}/${dname2} ${RABBITMQ_LOG_DIR}/archive${dname2}
		doarchive ${RABBITMQ_LOG_DIR}/${dname2} ${RABBITMQ_LOG_DIR}/archive${dname2} || rc=$?
		[ $rc -ne 0 ] && doerror $rc "abort from doarchive"

	done
	
	popd >/dev/null
done
[ ${DEBUG} -ne 0 ] && echo "================================================================================================================="

#
exit 0
